package com.enroll.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@Builder
public class SemesterClassInfoDto {
    private Long id;
    private Long semesterId;
    private String semesterName;
    private Long classId;
    private String className;
    private Integer classCredits;
    private Integer semesterCredits;
}
