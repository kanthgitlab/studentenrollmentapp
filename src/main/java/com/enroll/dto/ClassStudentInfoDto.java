package com.enroll.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@Builder
public class ClassStudentInfoDto {
    private Long id;
    private Long classId;
    private String className;
    private Long studentId;
    private String studentName;
    private Integer classCredits;
    private Integer studentCredits;
    private Long semesterId;
    private String semesterName;

}
