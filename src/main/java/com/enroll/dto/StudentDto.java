package com.enroll.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@Builder
public class StudentDto {
    private Long id;
    private String email;
    private String name;
    private String phoneNumber;
    private SemesterDto semester;
    private Integer credits;
}
