package com.enroll.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SemesterStudentDto {
    private Long id;
    private Long semesterId;
    private String semesterName;
    private Long studentId;
    private String studentName;
    private Integer semesterCredits;
    private Integer studentCredits;
}
