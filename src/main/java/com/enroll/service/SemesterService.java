package com.enroll.service;

import com.enroll.dao.ClassInfoRepository;
import com.enroll.dao.SemesterClassInfoRepository;
import com.enroll.dao.SemesterRepository;
import com.enroll.dto.ClassInfoDto;
import com.enroll.dto.SemesterDto;
import com.enroll.model.ClassInfo;
import com.enroll.model.Semester;
import com.enroll.model.SemesterClassInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SemesterService {

    private SemesterRepository semesterRepository;

    private ClassInfoRepository classInfoRepository;

    private SemesterClassInfoRepository semesterClassInfoRepository;

    private ClassInfoService classInfoService;

    @Autowired
    public SemesterService(SemesterRepository _semesterRepository,
                           ClassInfoRepository _classInfoRepository,
                           SemesterClassInfoRepository _semesterClassInfoRepository,
                           ClassInfoService _classInfoService) {
        this.semesterRepository = _semesterRepository;
        this.classInfoRepository = _classInfoRepository;
        this.semesterClassInfoRepository = _semesterClassInfoRepository;
        this.classInfoService = _classInfoService;
    }

    public List<SemesterDto> findAllSemesters(){
        Iterable<Semester> allSemesters = this.semesterRepository.findAll();
        return ((List<Semester>) allSemesters).stream()
                .map(semester -> mapToDto(semester)).collect(Collectors.toList());
    }

    public SemesterDto findOneSemesterById(Long semesterId){
        return mapToDto(semesterRepository.findOne(semesterId));
    }

    private SemesterDto mapToDto(Semester _semester) {

        //load classes mapped to semester
        List<SemesterClassInfo> semesterClassInfos =
                semesterClassInfoRepository.findAllBySemesterId(_semester.getId());

        List<Long> classIds =
                semesterClassInfos.stream().map(semesterClassInfo -> semesterClassInfo.getClassId())
                .collect(Collectors.toList());

        List<ClassInfoDto> classInfoDtos = classInfoService.findAllClassInfosByIds(classIds);

       return SemesterDto.builder().id(_semester.getId()).name(_semester.getName())
               .credits(_semester.getCredits())
               .classes(classInfoDtos).build();
    }

    public SemesterDto saveSemester(SemesterDto _semesterDto){

        Semester semester;

        if(_semesterDto.getId() != null) {
            semester = semesterRepository.findOne(_semesterDto.getId());
            semester.setName(_semesterDto.getName());
            //Integer semesterCredits = getSemesterCredits(_semesterDto.getId());
            //semester.setCredits(semesterCredits);
        }else{
            semester = Semester.builder().name(_semesterDto.getName())
                    .credits(0)
                    .build();
        }

        if(semester.getId() == null || semester.getId() == 0){
            semester.setCreatedDate(LocalDateTime.now());
        }
        if(semester.getId() != null && semester.getId() != 0){
            semester.setUpdatedDate(LocalDateTime.now());
        }

        return mapToDto(this.semesterRepository.save(semester));
    }

    public Semester updateSemesterCredits(Long semesterId, Integer semesterCredits){
        Semester semester = semesterRepository.findOne(semesterId);
        semester.setCredits(semesterCredits);
        return semesterRepository.save(semester);
    }

    public Boolean recomputeSemesterCredits(Long semesterId){
       Integer semesterCredits = getSemesterCredits(semesterId);
       updateSemesterCredits(semesterId, semesterCredits);
       return  true;
    }

    public Integer getSemesterCredits(Long semesterId){

       List<SemesterClassInfo> semesterClassInfos =
               semesterClassInfoRepository.findAllBySemesterId(semesterId);

       List<Long> classInfoIds =
               semesterClassInfos.stream().map(semesterClassInfo -> semesterClassInfo.getClassId())
               .collect(Collectors.toList());

       List<ClassInfo> classInfos =
               classInfoIds.stream().map(classInfoId -> classInfoRepository.findOne(classInfoId))
               .collect(Collectors.toList());

       Integer semesterCredits = classInfos.stream() .reduce(0, (partialCreditResult, classStudentInfoDto) -> partialCreditResult + classStudentInfoDto.getCredits(),
               Integer::sum);

        return  semesterCredits;
    }

    public boolean deleteSemester(Long id){
        this.semesterRepository.delete(id);
        return true;
    }

}
