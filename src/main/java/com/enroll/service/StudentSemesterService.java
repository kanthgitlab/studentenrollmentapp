package com.enroll.service;

import com.enroll.dao.SemesterStudentRepository;
import com.enroll.dto.SemesterDto;
import com.enroll.dto.SemesterStudentDto;
import com.enroll.dto.StudentDto;
import com.enroll.model.SemesterStudent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentSemesterService {

    private SemesterService semesterService;

    private StudentService studentService;

    private SemesterStudentRepository semesterStudentRepository;

    @Autowired
    public StudentSemesterService(SemesterService _semesterService,
                                  StudentService _studentService,
                                  SemesterStudentRepository _semesterStudentRepository) {
        this.semesterService = _semesterService;
        this.studentService = _studentService;
        this.semesterStudentRepository = _semesterStudentRepository;
    }

    public List<SemesterStudentDto> findAllSemesterStudentDtos(){
        List<SemesterStudent> allSemesterStudents = this.semesterStudentRepository.findAll();
        return allSemesterStudents.stream()
                .map(semesterStudentDto -> mapToDto(semesterStudentDto)).collect(Collectors.toList());
    }

    private SemesterStudentDto mapToDto(SemesterStudent _semesterStudent) {

        //get Student Details
        StudentDto studentDto =
                studentService.findOneStudentById(_semesterStudent.getStudentId());

        SemesterDto semesterDto =
                semesterService.findOneSemesterById(_semesterStudent.getSemesterId());

        //get Semester Details

       return SemesterStudentDto.builder().semesterId(_semesterStudent.getSemesterId())
               .semesterName(semesterDto.getName())
               .studentName(studentDto.getName())
               .semesterCredits(semesterDto.getCredits())
               .studentId(_semesterStudent.getStudentId()).build();
    }

    public SemesterStudentDto saveSemesterStudentInfo(SemesterStudent _semesterStudent){
        if(_semesterStudent.getCreatedDate() == null) {
            _semesterStudent.setCreatedDate(LocalDateTime.now());
        }
        if(_semesterStudent.getId() != null && _semesterStudent.getId() != 0){
            _semesterStudent.setUpdatedDate(LocalDateTime.now());
        }
        return mapToDto(this.semesterStudentRepository.save(_semesterStudent));
    }

    public boolean deleteSemesterStudent(Long id){
        this.semesterStudentRepository.delete(id);
        return true;
    }

    public SemesterStudentDto addStudentToSemester(SemesterStudentDto semesterStudentDto) {

        //get SemesterId
        SemesterDto semesterDto =
                semesterService.findOneSemesterById(semesterStudentDto.getSemesterId());

        Long semesterId = semesterDto.getId();

        //get StudentId
        StudentDto studentDto =
                studentService.findOneStudentById(semesterStudentDto.getStudentId());

        Long studentId = studentDto.getId();

        //check If Student is already enrolled in Semester
        SemesterStudent semesterStudent =
                semesterStudentRepository.findOneBySemesterIdAndStudentId(semesterId, studentId);

        SemesterStudentDto semesterStudentDto_Created  = null;

            //If No, call saveSemesterStudentInfo
            if(semesterStudent==null){
                SemesterStudent  semesterStudentEntity =
                        SemesterStudent.builder().semesterId(semesterId)
                                                 .studentId(studentId).build();

                semesterStudentDto_Created = saveSemesterStudentInfo(semesterStudentEntity);
            }

            //TODO Else, throw Exception

        return semesterStudentDto_Created;
    }
}
