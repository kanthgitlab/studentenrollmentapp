package com.enroll.service;

import com.enroll.dao.SemesterRepository;
import com.enroll.dao.StudentRepository;
import com.enroll.dto.SemesterDto;
import com.enroll.dto.StudentDto;
import com.enroll.model.Semester;
import com.enroll.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentService {

    private StudentRepository studentRepository;

    private SemesterRepository semesterRepository;

    private StudentCreditAggregationService  studentCreditAggregationService;


    @Autowired
    public StudentService(StudentRepository _studentRepository,
                          SemesterRepository _semesterRepository,
                          StudentCreditAggregationService _studentCreditAggregationService) {
        this.studentRepository = _studentRepository;
        this.semesterRepository = _semesterRepository;
        this.studentCreditAggregationService = _studentCreditAggregationService;
    }

    public StudentDto findOneStudentById(Long studentId) {
        Student student = studentRepository.findOne(studentId);
        return  mapToDto(student);

    }

    public List<StudentDto> findAllStudents(){
        List<Student> allStudents = this.studentRepository.findAll();
        return allStudents.stream()
                .map(student -> mapToDto(student)).collect(Collectors.toList());
    }

    private StudentDto mapToDto(Student _student) {
        Semester semester = _student.getSemesterId()!=null ? semesterRepository.findOne(_student.getSemesterId()) : null;
        SemesterDto semesterDto = null;
        Integer studentCredits = 0;
        if(semester != null){
            _student.setSemesterId(_student.getSemesterId());
            semesterDto = SemesterDto.builder().id(semester.getId())
                                    .name(semester.getName()).build();

            //get total-credits of student in current semester
            studentCredits =
                    studentCreditAggregationService.getCurrentCreditsOfAStudentInCurrentSemester(_student.getId());
        }

        return StudentDto.builder().id(_student.getId())
                .email(_student.getEmail()).name(_student.getName())
                .phoneNumber(_student.getPhoneNumber())
                .credits(studentCredits)
                .semester(semesterDto).build();
    }

    public StudentDto saveStudent(StudentDto _studentDto){

        Student student;

        if(_studentDto.getId() != null) {
            student = studentRepository.findOne(_studentDto.getId());
            student.setName(_studentDto.getName());
            student.setEmail(_studentDto.getEmail());

            if(_studentDto.getSemester()!=null && _studentDto.getSemester().getId()!=null) {
                student.setSemesterId(_studentDto.getSemester().getId());
            }
        }else{
            student =  Student.builder().email(_studentDto.getEmail())
                    .name(_studentDto.getName())
                    .phoneNumber(_studentDto.getPhoneNumber())
                    .build();

            if(_studentDto.getSemester()!=null && _studentDto.getSemester().getId()!=null){
                student.setSemesterId(_studentDto.getSemester().getId());
            }
        }

        if(student.getCreatedDate() == null) {
            student.setCreatedDate(LocalDateTime.now());
        }
        if(student.getId() != null && student.getId() != 0){
            student.setUpdatedDate(LocalDateTime.now());
        }
        return mapToDto(this.studentRepository.save(student));
    }

    public boolean deleteStudent(Long id){
        this.studentRepository.delete(id);
        return true;
    }

    public Boolean updateSemesterInfo(Long studentId, Long semesterId) {

        //get Semester Info
        Semester semester = semesterRepository.findOne(semesterId);

        //get Student Info
        Student student = studentRepository.findOne(studentId);

        student.setSemesterId(semester.getId());

        studentRepository.save(student);

        return true;
    }
}
