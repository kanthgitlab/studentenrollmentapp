package com.enroll.service;

import com.enroll.dao.ClassStudentInfoRepository;
import com.enroll.dto.ClassInfoDto;
import com.enroll.dto.ClassStudentInfoDto;
import com.enroll.dto.SemesterDto;
import com.enroll.dto.StudentDto;
import com.enroll.exception.EnrollmentException;
import com.enroll.model.ClassStudentInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClassStudentInfoService {


    private ClassInfoService classInfoService;

    private StudentService studentService;

    private SemesterService semesterService;

    private StudentSemesterService studentSemesterService;

    private ClassStudentInfoRepository classStudentInfoRepository;

    @Autowired
    public ClassStudentInfoService(ClassInfoService _classInfoService,
                                   StudentService _studentService,
                                   SemesterService _semesterService,
                                   ClassStudentInfoRepository _classStudentInfoRepository) {
        this.classInfoService = _classInfoService;
        this.studentService = _studentService;
        this.semesterService = _semesterService;
        this.classStudentInfoRepository = _classStudentInfoRepository;
    }

    public List<ClassStudentInfoDto> findAllClassStudentInfos() {
        List<ClassStudentInfo> allClassStudentInfos = this.classStudentInfoRepository.findAll();
        return allClassStudentInfos.stream()
                .map(classStudentInfo -> mapToDto(classStudentInfo)).collect(Collectors.toList());
    }

    private ClassStudentInfoDto mapToDto(ClassStudentInfo _classStudentInfo) {

        ClassInfoDto classInfoDto = classInfoService.findOneByClassInfoId(_classStudentInfo.getClassId());

        StudentDto studentDto = studentService.findOneStudentById(_classStudentInfo.getStudentId());

        SemesterDto semesterDto = semesterService.findOneSemesterById(_classStudentInfo.getSemesterId());

        Integer studentCredits = getCurrentCreditsOfAStudentInASemester(semesterDto.getId(), studentDto.getId());

        System.out.println("student credits computed as: "+studentCredits);

        return ClassStudentInfoDto.builder()
                .id(_classStudentInfo.getId())
                .classId(_classStudentInfo.getClassId())
                .className(classInfoDto.getName())
                .classCredits(classInfoDto.getCredits())
                .studentId(studentDto.getId())
                .studentName(studentDto.getName())
                .studentCredits(studentCredits)
                .semesterId(semesterDto.getId())
                .semesterName(semesterDto.getName())
                .build();
    }

    public ClassStudentInfoDto saveClassStudentInfo(ClassStudentInfo _classStudentInfo) {
        if (_classStudentInfo.getCreatedDate() == null) {
            _classStudentInfo.setCreatedDate(LocalDateTime.now());
        }
        if (_classStudentInfo.getId() != null && _classStudentInfo.getId() != 0) {
            _classStudentInfo.setUpdatedDate(LocalDateTime.now());
        }
        return mapToDto(this.classStudentInfoRepository.save(_classStudentInfo));
    }

    public boolean deleteClassStudentInfoDto(Long id) {
        this.classStudentInfoRepository.delete(id);
        return true;
    }

    public boolean dropStudentFromAClassOfASemester(ClassStudentInfoDto classStudentInfoDto){

        Long semesterId = classStudentInfoDto.getSemesterId();

        Long classInfoId = classStudentInfoDto.getClassId();

        Long studentId = classStudentInfoDto.getStudentId();

        if(!hasStudentEnrolledInAClassOfASemester(semesterId, classInfoId, studentId)){
            throw new EnrollmentException("Unable to drop Student - Student migtht have already dropped or never enrolled in to the class");
        }

        //find studentClass mapping info
        ClassStudentInfo classStudentInfo =
                classStudentInfoRepository.findOneBySemesterIdAndClassIdAndStudentId(
                classStudentInfoDto.getSemesterId(),
                classStudentInfoDto.getClassId(), classStudentInfoDto.getStudentId());

        //delete mapping
        classStudentInfoRepository.delete(classStudentInfo.getId());

        return true;
    }

    public ClassStudentInfoDto enrollStudentToClassOfASemester(ClassStudentInfoDto classStudentInfoDto) {

        Long semesterId = classStudentInfoDto.getSemesterId();

        Long classInfoId = classStudentInfoDto.getClassId();

        Long studentId = classStudentInfoDto.getStudentId();

        if(hasStudentEnrolledInAClassOfASemester(semesterId, classInfoId, studentId)){
            throw new EnrollmentException("Student has already enrolled in to the class");
        }

        //get current StudentCredits in semester
        //If it exceeds 20, then throw Error
        Integer studentCredits = getCurrentCreditsOfAStudentInASemester(semesterId, studentId);

        //get class Credits of enrollable clas
        ClassInfoDto classInfoDto = classInfoService.findOneByClassInfoId(classInfoId);

        Integer probableTotalCreditsAfterEnrollment = studentCredits + classInfoDto.getCredits();

        if(probableTotalCreditsAfterEnrollment > 20){
            throw new EnrollmentException("Student is only allowed to be enrolled in a maximum of 20 credits for each semester");
        }

        //If No, call saveSemesterStudentInfo
            ClassStudentInfo classStudentInfoEntity =
                    ClassStudentInfo.builder().classId(classInfoId).semesterId(semesterId)
                            .studentId(studentId).build();

        StudentDto studentDto = studentService.findOneStudentById(studentId);

        if(studentDto.getSemester() == null){
            studentService.updateSemesterInfo(studentId, semesterId);
        }

        return saveClassStudentInfo(classStudentInfoEntity);
    }

    public List<ClassStudentInfoDto> listAllStudentsEnrolledInAClassOfASemester(Long classId, Long semesterId){

        List<ClassStudentInfo> classStudentInfos =
                classStudentInfoRepository.findAllBySemesterIdAndClassId(semesterId, classId);

        return classStudentInfos.stream()
                .map(classStudentInfo -> mapToDto(classStudentInfo))
                .collect(Collectors.toList());
    }

    public List<ClassStudentInfoDto> listEnrolledClassesOfAStudentInASemester(Long semesterId, Long studentId){
        List<ClassStudentInfo> classStudentInfos =
                classStudentInfoRepository.findAllBySemesterIdAndStudentId(semesterId, studentId);

        System.out.println("classStudentInfos extracted in : "+classStudentInfos);

        return classStudentInfos.stream()
                .map(classStudentInfo -> mapToDto(classStudentInfo))
                .collect(Collectors.toList());
    }

    public Integer getCurrentCreditsOfAStudentInAClassOfASemester(Long semsterId, Long classId, Long studentId){
        ClassStudentInfoDto classStudentInfoDto =
                getStudentEnrolledInAClassOfASemester(semsterId,classId,studentId);

        return classStudentInfoDto!=null ? classStudentInfoDto.getClassCredits() : 0;
    }

    public Integer getCurrentCreditsOfAStudentInASemester(Long semsterId, Long studentId){
        List<ClassStudentInfo> classStudentInfos =
                classStudentInfoRepository.findAllBySemesterIdAndStudentId(semsterId, studentId);

        Integer totalCreditsOfAStudentInASemester =
                classStudentInfos.stream().map(classStudentInfo -> classStudentInfo.getClassId()).collect(Collectors.toList())
                .stream().map(classId -> classInfoService.findOneByClassInfoId(classId))
                .reduce(0, (partialCreditResult,
                            classInfoDto) -> partialCreditResult + classInfoDto.getCredits(),
                        Integer::sum);

        return totalCreditsOfAStudentInASemester;
    }

    public Boolean hasStudentEnrolledInAClassOfASemester(Long semesterId , Long classId, Long studentId){

        ClassStudentInfo classStudentInfo =
                classStudentInfoRepository.findOneBySemesterIdAndClassIdAndStudentId(semesterId, classId, studentId);

        return classStudentInfo!=null;
    }


    public ClassStudentInfoDto getStudentEnrolledInAClassOfASemester(Long semesterId , Long classId, Long studentId){

        ClassStudentInfo classStudentInfo =
                classStudentInfoRepository.findOneBySemesterIdAndClassIdAndStudentId(semesterId, classId, studentId);

        return mapToDto(classStudentInfo);
    }
}
