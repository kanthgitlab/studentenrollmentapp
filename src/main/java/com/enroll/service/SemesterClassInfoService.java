package com.enroll.service;

import com.enroll.dao.ClassInfoRepository;
import com.enroll.dao.SemesterClassInfoRepository;
import com.enroll.dto.ClassInfoDto;
import com.enroll.dto.SemesterClassInfoDto;
import com.enroll.dto.SemesterDto;
import com.enroll.model.ClassInfo;
import com.enroll.model.SemesterClassInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SemesterClassInfoService {

    private SemesterService semesterService;

    private ClassInfoService classInfoService;

    private ClassInfoRepository classInfoRepository;

    private SemesterClassInfoRepository semesterClassInfoRepository;

    @Autowired
    public SemesterClassInfoService(
            SemesterService _semesterService,
            ClassInfoService _classInfoService,
            ClassInfoRepository _classInfoRepository,
            SemesterClassInfoRepository _semesterClassInfoRepository) {
        this.semesterService = _semesterService;
        this.classInfoService = _classInfoService;
        this.classInfoRepository = _classInfoRepository;
        this.semesterClassInfoRepository = _semesterClassInfoRepository;
    }

    public List<SemesterClassInfoDto> findAllSemesterClassInfos(){
        List<SemesterClassInfo> allSemesterClasses = this.semesterClassInfoRepository.findAll();
        return allSemesterClasses.stream()
                .map(semesterClassInfo -> mapToDto(semesterClassInfo)).collect(Collectors.toList());
    }

    private SemesterClassInfoDto mapToDto(SemesterClassInfo _semesterClassInfo) {

        //get ClassInfo
        ClassInfoDto classInfoDto =
                classInfoService.findOneByClassInfoId(_semesterClassInfo.getClassId());

        //get SemesterInfo
        SemesterDto semesterDto =
                semesterService.findOneSemesterById(_semesterClassInfo.getSemesterId());

       return SemesterClassInfoDto.builder().classId(_semesterClassInfo.getClassId())
               .className(classInfoDto.getName()).classCredits(classInfoDto.getCredits())
               .semesterId(_semesterClassInfo.getSemesterId())
               .semesterCredits(semesterDto.getCredits())
               .semesterName(semesterDto.getName())
               .build();
    }

    public SemesterClassInfoDto saveSemesterClassInfo(SemesterClassInfo _semesterClassInfo){
        if(_semesterClassInfo.getCreatedDate() == null) {
            _semesterClassInfo.setCreatedDate(LocalDateTime.now());
        }
        if(_semesterClassInfo.getId() != null && _semesterClassInfo.getId() != 0){
            _semesterClassInfo.setUpdatedDate(LocalDateTime.now());
        }
        return mapToDto(this.semesterClassInfoRepository.save(_semesterClassInfo));
    }

    public SemesterClassInfoDto addClassToSemester(SemesterClassInfoDto semesterClassInfoDto) {

        //get SemesterInfo
        SemesterDto semesterDto =
                semesterService.findOneSemesterById(semesterClassInfoDto.getSemesterId());

        Long semesterId = semesterDto.getId();

        //get ClassInfo
        ClassInfoDto classInfoDto =
                classInfoService.findOneByClassInfoId(semesterClassInfoDto.getClassId());

        Long classInfoId = classInfoDto.getId();

        //check If Class is already added in Semester
        SemesterClassInfo semesterClassInfo =
                semesterClassInfoRepository.findOneBySemesterIdAndClassId(semesterId, classInfoId);

        SemesterClassInfoDto semesterClassInfoDtoCreated = null;

        //If No, call saveSemesterStudentInfo
        if(semesterClassInfo==null){
            SemesterClassInfo semesterClassInfoEntity =
                    SemesterClassInfo.builder().semesterId(semesterId)
                            .classId(classInfoId).build();

             semesterClassInfoDtoCreated = saveSemesterClassInfo(semesterClassInfoEntity);

             //update credits in semester
             Integer semesterCredits = getSemesterCredits(semesterId);

             semesterService.updateSemesterCredits(semesterId, semesterCredits);
        }

        //TODO Else, throw Exception

        return semesterClassInfoDtoCreated;
    }


    public Integer getSemesterCredits(Long semesterId){

        List<SemesterClassInfo> semesterClassInfos =
                semesterClassInfoRepository.findAllBySemesterId(semesterId);

        List<Long> classInfoIds =
                semesterClassInfos.stream().map(semesterClassInfo -> semesterClassInfo.getClassId())
                        .collect(Collectors.toList());

        List<ClassInfo> classInfos =
                classInfoIds.stream().map(classInfoId -> classInfoRepository.findOne(classInfoId))
                        .collect(Collectors.toList());

        Integer semesterCredits = classInfos.stream() .reduce(0, (partialCreditResult, classStudentInfoDto) -> partialCreditResult + classStudentInfoDto.getCredits(),
                Integer::sum);

        return  semesterCredits;
    }


    public boolean deleteSemesterClassInfo(Long id){
        this.semesterClassInfoRepository.delete(id);
        return true;
    }

}
