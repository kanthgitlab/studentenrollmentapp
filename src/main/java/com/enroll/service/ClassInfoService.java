package com.enroll.service;

import com.enroll.dao.ClassInfoRepository;
import com.enroll.dao.ClassStudentInfoRepository;
import com.enroll.dao.SemesterClassInfoRepository;
import com.enroll.dto.ClassInfoDto;
import com.enroll.model.ClassInfo;
import com.enroll.model.ClassStudentInfo;
import com.enroll.model.SemesterClassInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClassInfoService {

    private ClassInfoRepository classInfoRepository;

    private ClassStudentInfoRepository classStudentInfoRepository;

    private SemesterClassInfoRepository semesterClassInfoRepository;

    @Autowired
    public ClassInfoService(ClassInfoRepository _classInfoRepository,
                            ClassStudentInfoRepository _classStudentInfoRepository,
                            SemesterClassInfoRepository _semesterClassInfoRepository) {
        this.classInfoRepository = _classInfoRepository;
        this.classStudentInfoRepository = _classStudentInfoRepository;
        this.semesterClassInfoRepository = _semesterClassInfoRepository;
    }

    public List<ClassInfoDto> findAllClassInfos(){
        List<ClassInfo> allClasses = this.classInfoRepository.findAll();

        return allClasses.stream()
                .map(classInfo -> mapToDto(classInfo)).collect(Collectors.toList());
    }

    public List<ClassInfoDto> findAllClassInfosByIds(List<Long> classInfoIds){
        return classInfoIds.stream().map(classInfoId -> findOneByClassInfoId(classInfoId))
                .collect(Collectors.toList());
    }

    public ClassInfoDto findOneByClassInfoId(Long classInfoId) {
        return mapToDto(classInfoRepository.findOne(classInfoId));
    }

    private ClassInfoDto mapToDto(ClassInfo _classInfo) {

       return  ClassInfoDto.builder().id(_classInfo.getId()).name(_classInfo.getName())
               .credits(_classInfo.getCredits()).build();
    }

    public ClassInfoDto saveClassInfo(ClassInfoDto _classInfoDto){

        ClassInfo classInfo;

        if(_classInfoDto.getId() != null) {
            classInfo = classInfoRepository.findOne(_classInfoDto.getId());
            classInfo.setCredits(_classInfoDto.getCredits());
            classInfo.setName(_classInfoDto.getName());
        }else{
            classInfo = ClassInfo.builder().name(_classInfoDto.getName())
                    .credits(_classInfoDto.getCredits())
                    .build();
        }

        if(classInfo.getId() == null || classInfo.getId() == 0) {
            classInfo.setCreatedDate(LocalDateTime.now());
        }

        if(classInfo.getId() != null && classInfo.getId() != 0){
            classInfo.setUpdatedDate(LocalDateTime.now());
        }

        return mapToDto(this.classInfoRepository.save(classInfo));
    }

    public Boolean deleteClassInfoDependants(Long classInfoId){

        //delete all SemesterClass Mappings
        List<ClassStudentInfo> classStudentInfos =
                classStudentInfoRepository.findAllByClassId(classInfoId);

        classStudentInfos.stream().forEach(classStudentInfo -> classStudentInfoRepository.delete(classStudentInfo.getId()));

        //delete all ClassStudent Mappings
        List<SemesterClassInfo> semesterClassInfos =
                semesterClassInfoRepository.findAllByClassId(classInfoId);

        semesterClassInfos.stream().forEach(semesterClassInfo -> semesterClassInfoRepository.delete(semesterClassInfo.getId()));

        //recompute semester Credits


        //recompute student credits


        return true;
    }

    public boolean deleteClassInfo(Long id){
        this.classInfoRepository.delete(id);
        return true;
    }
}
