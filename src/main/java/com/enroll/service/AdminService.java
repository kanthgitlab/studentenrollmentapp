package com.enroll.service;

import com.enroll.dao.AdminRepository;
import com.enroll.dto.AdminDto;
import com.enroll.model.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AdminService {

    private AdminRepository adminRepository;

    @Autowired
    public AdminService(AdminRepository _adminRepository) {
        this.adminRepository = _adminRepository;
    }

    public List<AdminDto> findAllAdmins(){
        List<Admin> allAdmins = this.adminRepository.findAll();
        return allAdmins.stream()
                .map(admin -> mapToDto(admin)).collect(Collectors.toList());
    }

    public AdminDto findOneAdminById(Long adminId) {
        Admin admin = adminRepository.findOne(adminId);
        return  mapToDto(admin);
    }

    private AdminDto mapToDto(Admin _admin) {
        return AdminDto.builder().id(_admin.getId())
                .email(_admin.getEmail()).name(_admin.getName())
                .phoneNumber(_admin.getPhoneNumber())
                .build();
    }

    public AdminDto saveAdmin(AdminDto _adminDto){

        Admin admin;

        if(_adminDto.getId() != null) {
            admin = adminRepository.findOne(_adminDto.getId());
            admin.setName(_adminDto.getName());
            admin.setEmail(_adminDto.getEmail());
            admin.setPhoneNumber(_adminDto.getPhoneNumber());
        }else{
            admin =  Admin.builder().email(_adminDto.getEmail())
                    .name(_adminDto.getName())
                    .phoneNumber(_adminDto.getPhoneNumber())
                    .build();
        }

        if(admin.getCreatedDate() == null) {
            admin.setCreatedDate(LocalDateTime.now());
        }
        if(admin.getId() != null && admin.getId() != 0){
            admin.setUpdatedDate(LocalDateTime.now());
        }
        return mapToDto(this.adminRepository.save(admin));
    }


    public boolean deleteAdmin(Long id){
        this.adminRepository.delete(id);
        return true;
    }
}
