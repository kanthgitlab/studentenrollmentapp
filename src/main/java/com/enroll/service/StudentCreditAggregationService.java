package com.enroll.service;

import com.enroll.dao.ClassInfoRepository;
import com.enroll.dao.ClassStudentInfoRepository;
import com.enroll.dao.StudentRepository;
import com.enroll.model.ClassInfo;
import com.enroll.model.ClassStudentInfo;
import com.enroll.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentCreditAggregationService {

    private StudentRepository studentRepository;

    private ClassStudentInfoRepository classStudentInfoRepository;

    private ClassInfoRepository classInfoRepository;

    @Autowired
    public StudentCreditAggregationService(
                    StudentRepository _studentRepository,
                    ClassStudentInfoRepository _classStudentInfoRepository,
                    ClassInfoRepository _classInfoRepository) {
        this.studentRepository = _studentRepository;
        this.classStudentInfoRepository = _classStudentInfoRepository;
        this.classInfoRepository = _classInfoRepository;
    }

    public Integer getCurrentCreditsOfAStudentInCurrentSemester(Long studentId){

        //get Student's semesterId
        Student student =
                studentRepository.findOne(studentId);

        Integer totalCreditsOfAStudentInASemester = 0;

        if(student.getSemesterId()!=null){

            List<ClassStudentInfo> classesOfStudent =
                    classStudentInfoRepository.findAllBySemesterIdAndStudentId(
                            student.getId(), studentId);

            List<Long> classIds =
                    classesOfStudent.stream()
                            .map(classStudentInfo -> classStudentInfo.getClassId())
                            .collect(Collectors.toList());

            if(classIds!=null && classIds.size() >0){

                List<ClassInfo> classes =
                        classIds.stream().map(classInfoId -> classInfoRepository.findOne(classInfoId))
                                .collect(Collectors.toList());

                totalCreditsOfAStudentInASemester = classes.stream()
                        .reduce(0, (partialCreditResult,
                                    classInfo) -> partialCreditResult + classInfo.getCredits(),
                                Integer::sum);
            }

        }

        return totalCreditsOfAStudentInASemester;
    }
}
