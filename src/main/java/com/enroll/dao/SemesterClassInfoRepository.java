package com.enroll.dao;

import com.enroll.model.SemesterClassInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface SemesterClassInfoRepository extends JpaRepository<SemesterClassInfo, Long> {

    /**
     * extract All SemesterClassInfos by SemesterId
     *
     * @return List<SemesterClassInfo>
     */
    public List<SemesterClassInfo> findAllBySemesterId(Long semesterId);

    /**
     * extract All SemesterClassInfos by classId
     *
     * @return List<SemesterClassInfo>
     */
    public List<SemesterClassInfo> findAllByClassId(Long classId);

    SemesterClassInfo findOneBySemesterIdAndClassId(Long semesterId, Long classInfoId);
}
