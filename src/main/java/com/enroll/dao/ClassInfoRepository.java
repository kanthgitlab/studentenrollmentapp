package com.enroll.dao;

import com.enroll.model.ClassInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface ClassInfoRepository extends JpaRepository<ClassInfo, Long> {

}
