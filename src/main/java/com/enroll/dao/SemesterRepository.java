package com.enroll.dao;

import com.enroll.model.Semester;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface SemesterRepository extends JpaRepository<Semester, Long> {

}
