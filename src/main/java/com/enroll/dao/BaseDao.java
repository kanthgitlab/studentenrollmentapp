package com.enroll.dao;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;

public class BaseDao {

    @Autowired
    protected EntityManager entityManager;
}
