package com.enroll.dao;

import com.enroll.model.ClassStudentInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface ClassStudentInfoRepository extends JpaRepository<ClassStudentInfo, Long> {

    //list of students enrolled in a class for a particular semester
    public List<ClassStudentInfo> findAllBySemesterIdAndClassId(Long semesterId, Long classId);

    //list of classes that a student has enrolled in a particular semester
    public List<ClassStudentInfo> findAllBySemesterIdAndStudentId(Long semesterId, Long studentId);

    //details of a student enrolled in a class for a particular semester
    public ClassStudentInfo findOneBySemesterIdAndClassIdAndStudentId(Long semesterId, Long classId, Long studentId);

    //list of Students enrolled in all semesters for a particular ClassId
    public List<ClassStudentInfo> findAllByClassId(Long classId);

    //List of Classes across all semesters that student has enrolled
    public List<ClassStudentInfo> findAllByStudentId(Long studentId);
}
