package com.enroll.dao;

import com.enroll.model.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface AdminRepository extends JpaRepository<Admin, Long> {

}
