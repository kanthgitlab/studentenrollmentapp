package com.enroll.dao;

import com.enroll.model.SemesterStudent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface SemesterStudentRepository extends JpaRepository<SemesterStudent, Long> {

    /**
     * extract All SemesterStudents by SemesterId
     *
     * @return List<SemesterStudent>
     */
    public List<SemesterStudent> findAllBySemesterId(Long semesterId);

    /**
     * extract All SemesterStudents by studentId
     *
     * @return List<SemesterStudent>
     */
    public List<SemesterStudent> findAllByStudentId(Long studentId);

    public SemesterStudent findOneBySemesterIdAndStudentId(Long semesterId, Long studentId);

}
