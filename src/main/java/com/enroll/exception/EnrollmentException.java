package com.enroll.exception;


public class EnrollmentException extends RuntimeException {

    public EnrollmentException(String message) {

        super(message);
    }
}