package com.enroll.web;

import com.enroll.dto.SemesterClassInfoDto;
import com.enroll.dto.SemesterDto;
import com.enroll.service.SemesterClassInfoService;
import com.enroll.service.SemesterService;
import com.enroll.service.StudentSemesterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/api/semester")
public class SemesterController {

    private SemesterService semesterService;

    private StudentSemesterService studentSemesterService;

    private SemesterClassInfoService semesterClassInfoService;


    @Autowired
    public SemesterController(SemesterService _semesterService,
                       StudentSemesterService _studentSemesterService,
                       SemesterClassInfoService _semesterClassInfoService) {
        this.semesterService = _semesterService;
        this.studentSemesterService = _studentSemesterService;
        this.semesterClassInfoService = _semesterClassInfoService;
    }

    @GetMapping("/fetchAll")
    @ResponseBody
    public List<SemesterDto> list() {
        return semesterService.findAllSemesters();
    }

    @GetMapping("/fetch")
    @ResponseBody
    public SemesterDto fetchASemesterById(@RequestParam Long semesterId) {
        return semesterService.findOneSemesterById(semesterId);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public SemesterDto addSemester(@RequestBody SemesterDto semesterDto) {
       return semesterService.saveSemester(semesterDto);
    }

    @ResponseBody
    @DeleteMapping
    public boolean delete(long id) {
        return semesterService.deleteSemester(id);
    }

    @RequestMapping(value="/addClass", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public SemesterClassInfoDto addClassToSemester(@RequestBody SemesterClassInfoDto semesterClassInfoDto) {
       return semesterClassInfoService.addClassToSemester(semesterClassInfoDto);
    }

}
