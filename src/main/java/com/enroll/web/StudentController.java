package com.enroll.web;

import com.enroll.dto.StudentDto;
import com.enroll.exception.EnrollmentException;
import com.enroll.service.StudentCreditAggregationService;
import com.enroll.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/student")
public class StudentController {

    private StudentService studentService;

    private StudentCreditAggregationService studentCreditAggregationService;

    @Autowired
    StudentController(StudentService _studentService,
                      StudentCreditAggregationService _studentCreditAggregationService) {
        this.studentService = _studentService;
        this.studentCreditAggregationService = _studentCreditAggregationService;
    }

    @GetMapping("/fetchAll")
    @ResponseBody
    public Iterable<StudentDto> list() {
        return studentService.findAllStudents();
    }

    @GetMapping("/fetch")
    @ResponseBody
    public StudentDto fetchAStudentById(@RequestParam Long studentId) {
        StudentDto studentDto = studentService.findOneStudentById(studentId);
        Integer credits = studentCreditAggregationService.getCurrentCreditsOfAStudentInCurrentSemester(studentId);
        studentDto.setCredits(credits);
        return studentDto;
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public StudentDto addStudent(@RequestBody StudentDto studentDto) throws EnrollmentException{
        StudentDto newStudent = studentService.saveStudent(studentDto);
        Integer credits = studentCreditAggregationService.getCurrentCreditsOfAStudentInCurrentSemester(newStudent.getId());
        newStudent.setCredits(credits);
        return newStudent;
    }

    @ResponseBody
    @DeleteMapping
    public boolean delete(long id) {
        return studentService.deleteStudent(id);
    }

}
