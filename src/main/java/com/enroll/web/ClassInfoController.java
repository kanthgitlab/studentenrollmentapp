package com.enroll.web;

import com.enroll.dto.ClassInfoDto;
import com.enroll.service.ClassInfoService;
import com.enroll.service.ClassStudentInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/api/class")
public class ClassInfoController {

    private ClassInfoService classInfoService;

    private ClassStudentInfoService classStudentInfoService;

    @Autowired
    ClassInfoController(ClassInfoService _classInfoService, ClassStudentInfoService _classStudentInfoService) {
        this.classInfoService = _classInfoService;
        this.classStudentInfoService = _classStudentInfoService;
    }

    @GetMapping("/fetchAll")
    @ResponseBody
    public List<ClassInfoDto> list() {
        return classInfoService.findAllClassInfos();
    }

    @GetMapping("/fetch")
    @ResponseBody
    public ClassInfoDto fetchAClassInfoById(@RequestParam Long classInfoId) {
        return classInfoService.findOneByClassInfoId(classInfoId);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public ClassInfoDto addClass(@RequestBody ClassInfoDto classInfoDto) {
        return classInfoService.saveClassInfo(classInfoDto);
    }

    @ResponseBody
    @DeleteMapping
    public boolean delete(long id) {
        return classInfoService.deleteClassInfo(id);
    }


}
