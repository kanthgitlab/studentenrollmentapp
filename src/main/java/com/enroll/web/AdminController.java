package com.enroll.web;

import com.enroll.dto.AdminDto;
import com.enroll.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/api/admin")
public class AdminController {

    private AdminService adminService;

    @Autowired
    AdminController(AdminService _adminService) {
        this.adminService = _adminService;
    }

    @GetMapping("/fetchAll")
    @ResponseBody
    public List<AdminDto> list() {
        return adminService.findAllAdmins();
    }

    @GetMapping("/fetch")
    @ResponseBody
    public AdminDto fetchAnAdminById(@RequestParam Long adminId) {
        return adminService.findOneAdminById(adminId);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public boolean addAdmin(@RequestBody AdminDto adminDto) {
        AdminDto newAdmin = adminService.saveAdmin(adminDto);
        return newAdmin.getId() != null;
    }

    @ResponseBody
    @DeleteMapping
    public boolean delete(long id) {
        return adminService.deleteAdmin(id);
    }

}
