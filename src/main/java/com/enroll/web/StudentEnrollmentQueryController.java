package com.enroll.web;

import com.enroll.dto.ClassStudentInfoDto;
import com.enroll.dto.StudentDto;
import com.enroll.service.ClassStudentInfoService;
import com.enroll.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/api/fetch")
public class StudentEnrollmentQueryController {

    private ClassStudentInfoService classStudentInfoService;
    private StudentService studentService;


    @Autowired
    StudentEnrollmentQueryController(ClassStudentInfoService _classStudentInfoService,
                                     StudentService _studentService) {
        this.classStudentInfoService = _classStudentInfoService;
        this.studentService = _studentService;
    }


    @GetMapping(value = "/byId")
    @ResponseBody
    public StudentDto getStudentDetails(@RequestParam Long studentId) {
        return studentService.findOneStudentById(studentId);
    }

    @GetMapping(value = "/one")
    @ResponseBody
    public ClassStudentInfoDto getStudentDetailsOfAClassInASemester(@RequestParam Long studentId,
                                                                    @RequestParam Long classId,
                                                                    @RequestParam Long semesterId) {
        return classStudentInfoService.getStudentEnrolledInAClassOfASemester(semesterId,classId,studentId);
    }

    @GetMapping(value = "/allInClass")
    @ResponseBody
    public List<ClassStudentInfoDto> listStudentsInAClassOfASemester(@RequestParam Long classId,
                                                                     @RequestParam Long semesterId) {
        return classStudentInfoService.listAllStudentsEnrolledInAClassOfASemester(classId,semesterId);
    }

    @GetMapping(value = "/allClasses")
    @ResponseBody
    public List<ClassStudentInfoDto> listStudentsEnrolledClassesOfASemester(@RequestParam Long studentId,
                                                                    @RequestParam Long semesterId) {
        return classStudentInfoService.listEnrolledClassesOfAStudentInASemester(semesterId,studentId);
    }
}
