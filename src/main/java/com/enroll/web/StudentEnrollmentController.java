package com.enroll.web;

import com.enroll.dto.ClassStudentInfoDto;
import com.enroll.service.ClassStudentInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/api/enrollment")
public class StudentEnrollmentController {

    private ClassStudentInfoService classStudentInfoService;

    @Autowired
    StudentEnrollmentController(ClassStudentInfoService _classStudentInfoService) {
        this.classStudentInfoService = _classStudentInfoService;
    }

    @GetMapping("/fetchAll")
    @ResponseBody
    public List<ClassStudentInfoDto> list() {
        return classStudentInfoService.findAllClassStudentInfos();
    }

    @GetMapping("/fetch")
    @ResponseBody
    public ClassStudentInfoDto getStudentDetailsOfAClassInASemester(@RequestParam Long semesterId,@RequestParam Long classId,
                                                            @RequestParam Long studentId) {
        return classStudentInfoService.getStudentEnrolledInAClassOfASemester(semesterId, classId, studentId);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public ClassStudentInfoDto enrollStudent(@RequestBody ClassStudentInfoDto classStudentInfoDto) {
       return classStudentInfoService.enrollStudentToClassOfASemester(classStudentInfoDto);
    }

    @RequestMapping(value = "/drop", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public Boolean dropStudent(@RequestBody ClassStudentInfoDto classStudentInfoDto) {
        return classStudentInfoService.dropStudentFromAClassOfASemester(classStudentInfoDto);
    }

}
