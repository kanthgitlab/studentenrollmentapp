# deploy instructions for JAR file to EC2

## Local packaging before deploying to EC2:

```shell
 mvn package spring-boot:repackage
```

## Install java 1.8 on EC2:

### remove java-7

```shell
sudo yum remove java-1.7.0-openjdk
```

### install java-8

```shell
sudo yum install java-1.8.0
```

## Connect to EC2:

```shell
ssh -i ~/Documents/aws/lakshmi.pem ec2-user@ec2-13-xx-xx-114.ap-southeast-1.compute.amazonaws.com
```

## SCP jar file to EC2:

```shell
scp -i ~/Documents/aws/lakshmi.pem studentenrollmentapp-0.0.1-SNAPSHOT-spring-boot.jar ec2-user@ec2-xx-xx-xx-114.ap-southeast-1.compute.amazonaws.com:~
```

## scp mysql docker file to EC2:

```shell
scp -i ~/Documents/aws/lakshmi.pem ~/Desktop/blockone/studentenrollmentapp/mysql/docker-compose.yml ec2-user@ec2-13-xx-xx-114.ap-southeast-1.compute.amazonaws.com:~/mysql
```

## start mysql docker on remote:

```shell
cd ~/studentenrollmentapp/mysql
docker-compose up -d
```

## start application

```shell
cd ~
java -jar studentenrollmentapp-0.0.1-SNAPSHOT-spring-boot.jar
```
