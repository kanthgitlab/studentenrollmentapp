# How to start MySQL on local docker instance

-  Docker compose file for MySQL can be located under directory mysql

- Navigate to mysql directory

```shell
cd mysql
```

- To start docker instance of mysql, Please run command:

```shell
docker-compose up -d
```

- To get the containerId of mySQL process, run command:

```shell
docker ps -a
```

- Identify containerId of the mysql container in output of above command

```
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
d76543b5827b        mariadb:10.5.5      "docker-entrypoint.s…"   5 seconds ago       Up 3 seconds        0.0.0.0:3306->3306/tcp   mysql_mysqldb_1

```

- To verify logs Please execute 2 commands:

```shell
docker logs d76543b5827b --follow
```

```
Creating network "mysql_common-network" with driver "bridge" Creating
2020-10-25 08:04:45+00:00 [Note] [Entrypoint]: Entrypoint script for
MySQL Server 1:10.5.5+maria~focal started. 2020-10-25 08:04:45+00:00
[Note] [Entrypoint]: Switching to dedicated user 'mysql' 2020-10-25
08:04:45+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server
1:10.5.5+maria~focal started. 2020-10-25 08:04:45+00:00 [Note]
[Entrypoint]: Initializing database files lakshmikanth-MacBook-Pro:mysql
lakshmikanth$ docker logs d76543b5827b 2020-10-25 08:04:45+00:00 [Note]
[Entrypoint]: Entrypoint script for MySQL Server 1:10.5.5+maria~focal
started. 2020-10-25 08:04:45+00:00 [Note] [Entrypoint]: Switching to
dedicated user 'mysql' 2020-10-25 08:04:45+00:00 [Note] [Entrypoint]:
Entrypoint script for MySQL Server 1:10.5.5+maria~focal started.
2020-10-25 08:04:45+00:00 [Note] [Entrypoint]: Initializing database
files lakshmikanth-MacBook-Pro:mysql lakshmikanth$ docker logs
d76543b5827b 2020-10-25 08:04:45+00:00 [Note] [Entrypoint]: Entrypoint
script for MySQL Server 1:10.5.5+maria~focal started. 2020-10-25
08:04:45+00:00 [Note] [Entrypoint]: Switching to dedicated user 'mysql'
2020-10-25 08:04:45+00:00 [Note] [Entrypoint]: Entrypoint script for
MySQL Server 1:10.5.5+maria~focal started. 2020-10-25 08:04:45+00:00
[Note] [Entrypoint]: Initializing database files
lakshmikanth-MacBook-Pro:mysql lakshmikanth$ docker logs d76543b5827b
--follow 2020-10-25 08:04:45+00:00 [Note] [Entrypoint]: Entrypoint
script for MySQL Server 1:10.5.5+maria~focal started. 2020-10-25
08:04:45+00:00 [Note] [Entrypoint]: Switching to dedicated user 'mysql'
2020-10-25 08:04:45+00:00 [Note] [Entrypoint]: Entrypoint script for
MySQL Server 1:10.5.5+maria~focal started. 2020-10-25 08:04:45+00:00
[Note] [Entrypoint]: Initializing database files


PLEASE REMEMBER TO SET A PASSWORD FOR THE MariaDB root USER !
To do so, start the server, then issue the following commands:

'/usr/bin/mysqladmin' -u root password 'new-password'
'/usr/bin/mysqladmin' -u root -h  password 'new-password'

Alternatively you can run:
'/usr/bin/mysql_secure_installation'

which will also give you the option of removing the test
databases and anonymous user created by default.  This is
strongly recommended for production servers.

See the MariaDB Knowledgebase at https://mariadb.com/kb or the
MySQL manual for more instructions.

Please report any problems at https://mariadb.org/jira

The latest information about MariaDB is available at https://mariadb.org/.
You can find additional information about the MySQL part at:
https://dev.mysql.com
Consider joining MariaDB's strong and vibrant community:
https://mariadb.org/get-involved/

2020-10-25 08:05:11+00:00 [Note] [Entrypoint]: Database files initialized
2020-10-25 08:05:11+00:00 [Note] [Entrypoint]: Starting temporary server
2020-10-25 08:05:11+00:00 [Note] [Entrypoint]: Waiting for server startup
2020-10-25  8:05:11 0 [Note] mysqld (mysqld 10.5.5-MariaDB-1:10.5.5+maria~focal) starting as process 107 ...
2020-10-25  8:05:11 0 [Note] InnoDB: Using Linux native AIO
2020-10-25  8:05:11 0 [Note] InnoDB: Uses event mutexes
2020-10-25  8:05:11 0 [Note] InnoDB: Compressed tables use zlib 1.2.11
2020-10-25  8:05:11 0 [Note] InnoDB: Number of pools: 1
2020-10-25  8:05:11 0 [Note] InnoDB: Using SSE4.2 crc32 instructions
2020-10-25  8:05:11 0 [Note] mysqld: O_TMPFILE is not supported on /tmp (disabling future attempts)
2020-10-25  8:05:11 0 [Note] InnoDB: Initializing buffer pool, total size = 134217728, chunk size = 134217728
2020-10-25  8:05:11 0 [Note] InnoDB: Completed initialization of buffer pool
2020-10-25  8:05:11 0 [Note] InnoDB: If the mysqld execution user is authorized, page cleaner thread priority can be changed. See the man page of setpriority().
2020-10-25  8:05:11 0 [Note] InnoDB: 128 rollback segments are active.
2020-10-25  8:05:11 0 [Note] InnoDB: Creating shared tablespace for temporary tables
2020-10-25  8:05:11 0 [Note] InnoDB: Setting file './ibtmp1' size to 12 MB. Physically writing the file full; Please wait ...
2020-10-25  8:05:13 0 [Note] InnoDB: File './ibtmp1' size is now 12 MB.
2020-10-25  8:05:13 0 [Note] InnoDB: 10.5.5 started; log sequence number 45051; transaction id 21
2020-10-25  8:05:13 0 [Note] Plugin 'FEEDBACK' is disabled.
2020-10-25  8:05:13 0 [Note] InnoDB: Loading buffer pool(s) from /var/lib/mysql/ib_buffer_pool
2020-10-25  8:05:13 0 [Note] InnoDB: Buffer pool(s) load completed at 201025  8:05:13
2020-10-25  8:05:13 0 [Warning] 'user' entry 'root@d76543b5827b' ignored in --skip-name-resolve mode.
2020-10-25  8:05:13 0 [Warning] 'user' entry '@d76543b5827b' ignored in --skip-name-resolve mode.
2020-10-25  8:05:13 0 [Warning] 'proxies_priv' entry '@% root@d76543b5827b' ignored in --skip-name-resolve mode.
2020-10-25  8:05:13 0 [Note] Reading of all Master_info entries succeeded
2020-10-25  8:05:13 0 [Note] Added new Master_info '' to hash table
2020-10-25  8:05:13 0 [Note] mysqld: ready for connections.
Version: '10.5.5-MariaDB-1:10.5.5+maria~focal'  socket: '/run/mysqld/mysqld.sock'  port: 0  mariadb.org binary distribution
2020-10-25 08:05:14+00:00 [Note] [Entrypoint]: Temporary server started.
Warning: Unable to load '/usr/share/zoneinfo/leap-seconds.list' as time zone. Skipping it.
Warning: Unable to load '/usr/share/zoneinfo/leapseconds' as time zone. Skipping it.
```
